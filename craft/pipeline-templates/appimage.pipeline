// Sometimes we need to include additional parameters to Craft which are specific to the project
def craftProjectParams = ""

// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfig = "binary-factory-tooling/craft/configs/master/CraftBinaryCache.ini"

// Determine if we need to build a specific version of this project
if( craftTarget != '' ) {
	// If we have a specified version (Craft target) then we need to pass this along to Craft
	craftProjectParams = "--target ${craftTarget}"
}

// Request a node to be allocated to us
node( "Appimage" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// Make sure the Git checkouts are up to date
			sh """
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository git://anongit.kde.org/craftmaster --into ~/Craft/BinaryFactory/
				python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository git://anongit.kde.org/sysadmin/binary-factory-tooling --into ~/Craft/BinaryFactory/
			"""

			// Update Craft itself
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c -i craft
			"""
		}

		stage('Installing Dependencies') {
			// Ask Craftmaster to ensure all the dependencies are installed for this application we are going to be building
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --install-deps ${craftBuildBlueprint}
			"""
		}

		stage('Building') {
			// Actually build the application now
			sh """
				cd ~/Craft/BinaryFactory/
				python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --no-cache ${craftProjectParams} ${craftRebuildBlueprint}
			"""
		}

		stage('Packaging') {
			// Now generate the appimage for it
			sh """
				cd ~/Craft/BinaryFactory/
				// If Craft knew how to generate Appimages we could just run the below...
				//python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --package ${craftBuildBlueprint}
				// Instead we'll have to ask Craft to stage all the resources like it normally would, then we can run the necessary external tools to generate the appimage
			"""

			// Now copy it to the Jenkins workspace so it can be grabbed from there
			sh """
				cd ~/Craft/BinaryFactory/
				packageDir=\$(python3 "craftmaster/Craftmaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base)

				cp -vf \$packageDir/* \$WORKSPACE/
			"""
		}

		stage('Capturing Package') {
			// Then we ask Jenkins to capture the generated installers as an artifact
			archiveArtifacts artifacts: '*.appimage, *.sha256', onlyIfSuccessful: true
		}
	}
}
}

