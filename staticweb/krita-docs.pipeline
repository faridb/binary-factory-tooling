// Request a node to be allocated to us
node( "StaticWeb" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
        // First Thing: Checkout Sources
        stage('Checkout Sources') {
            // Make sure we have a clean slate to begin with
            deleteDir()
            // Code
            checkout changelog: true, poll: true, scm: [
                $class: 'GitSCM',
                branches: [[name: gitBranch]],
                userRemoteConfigs: [[url: gitUrl]]
            ]
        }

        // Let's build website now
        stage('Website build') {
            sh """
                mkdir \$WORKSPACE/_site/
                
                export LC_ALL=C.UTF-8
                sphinx-intl build
                
                LANGUAGES="en de fr nl uk_UA zh_CN ca pt_PT"
                for langToBuild in \$LANGUAGES; do
                    sphinx-build -M html \$WORKSPACE/ \$WORKSPACE/staging/\$langToBuild/ -D language="\$langToBuild"
                    sphinx-build -M epub \$WORKSPACE/ \$WORKSPACE/staging/\$langToBuild/ -D language="\$langToBuild"

                    mv $WORKSPACE/staging/\$langToBuild/html/ $WORKSPACE/_site/\$langToBuild/
                    mkdir $WORKSPACE/_site/\$langToBuild/epub/
                    mv $WORKSPACE/staging/\$langToBuild/epub/KritaManual.epub $WORKSPACE/_site/\$langToBuild/epub/
                done
            """
        }

        // Deploy the website!
        stage('Publishing Website') {
            sh """
                rsync -Hav --delete -e "ssh -i $HOME/WebsitePublishing/website-upload.key" _site/ sitedeployer@olios.kde.org:${deploypath}
            """
        }
    }
}
}
