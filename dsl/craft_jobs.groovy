// Read the contents of the gathered-jobs.json file a step created for us previously
def jobsToParse = readFileFromWorkspace('craft/gathered-jobs.json')
def knownJobs = new groovy.json.JsonSlurper().parseText( jobsToParse )

// Iterate over all of the known jobs and create them!
knownJobs.each {
	// Create our job name
	def jobName = "${it.project}_${it.targetName}_${it.platform}"

	// Read in the necessary Pipeline template
	def pipelineTemplate = readFileFromWorkspace("craft/pipeline-templates/${it.platform}.pipeline")
	// Now we can construct our Pipeline script
	// We append a series of variables to the top of it to provide a variety of useful information to the otherwise templated script
	// These appended variables are what makes one build different to the next, aside from the template which was used
	def pipelineScript = """
		|def project = "${it.project}"
		|def craftTarget = "${it.target}"
		|def craftPlatform = "${it.craftPlatform}"
		|def craftBuildBlueprint = "${it.buildBlueprint}"
		|def craftRebuildBlueprint = "${it.rebuildBlueprint}"
		|def craftOptions = "${it.options}"
		|def craftPackageAppx = "${it.packageAppx}"

		|${pipelineTemplate}""".stripMargin()

	// Actually create the job now
	pipelineJob( jobName ) {
		properties {
			// We don't want to keep build results forever
			// We'll set it to keep the last 10 builds and discard everything else
			buildDiscarder {
				strategy {
					logRotator {
						numToKeepStr("10")
						daysToKeepStr('')
						artifactDaysToKeepStr('')
						artifactNumToKeepStr('')
					}
				}
			}
			// We don't want to be building the same project more than once
			// This is to prevent one project hogging resources
			// And also has a practical component as otherwise an older build could finish afterwards and upload old build results
			disableConcurrentBuilds()
		}
		triggers {
			// We want to automatically rebuild once a day
			cron("@daily")
		}
		// This is where the Pipeline script actually happens :)
		definition {
			cps {
				script( pipelineScript )
				sandbox()
			}
		}
	}
}
